<?php

declare(strict_types=1);

namespace App\Forms;

use Nette\Application\UI\Form;
use Nette\Security\User;
use stdClass;

final class LoginFormFactory
{
    public function __construct(
        protected User $user,
    ) {
    }

    public function create(): Form
    {
        $form = new Form();

        $form->addText('email', 'Email')
            ->setMaxLength(255)
            ->setRequired('Please provide your email.');

        $form->addPassword('password', 'Password:')
            ->setRequired('Please enter your password.');

        $form->addSubmit('send', 'Sign in');

        $form->onSuccess[] = function (Form $form, stdClass $data) {
            $this->user->login($data->email, $data->password);
        };

        return $form;
    }
}
