<?php

declare(strict_types=1);

namespace App\Forms;

use App\Model\UserFacade;
use Nette\Application\UI\Form;
use Nette\Security\User;
use stdClass;

final class RegistrationFormFactory
{
    public function __construct(
        protected UserFacade $userFacade,
    ) {
    }

    public function create(): Form
    {
        $form = new Form();

        $form->addText('email', 'Email')
            ->setMaxLength(255)
            ->setRequired('Please provide your email.');

        $form->addPassword('password', 'Password:')
            ->setRequired('Please enter your password.');

        $form->addSubmit('send', 'Create an account');

        $form->onSuccess[] = function (Form $form, stdClass $data) {
            $this->userFacade->registerUser($data->email, $data->password);
        };

        return $form;
    }
}
