<?php

declare(strict_types=1);

namespace App\Forms;

use App\Model\UserFacade;
use Nette\Application\UI\Form;
use stdClass;

final class ChangePasswordFormFactory
{
    public function __construct(
        protected UserFacade $userFacade,
    ) {
    }

    public function create(): Form
    {
        $form = new Form();

        // todo. force login

        $form->addPassword('oldPassword', 'Old password:')
            ->setRequired('Please enter your old password.');

        $form->addPassword('newPassword', 'New password:')
            ->setRequired('Please enter your new password.');

        $form->addSubmit('send', 'Change password');

        $form->onSuccess[] = function (Form $form, stdClass $data) {
            $this->userFacade->changePassword(0, $data->newPassword);
        };

        return $form;
    }
}
