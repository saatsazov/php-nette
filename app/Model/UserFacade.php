<?php

declare(strict_types=1);

namespace App\Model;

use Nette\Database\Connection;
use Nette\Database\Explorer;
use Nette\Security\AuthenticationException;
use Nette\Security\Authenticator;
use Nette\Security\Passwords;
use Nette\Security\SimpleIdentity;

final class UserFacade implements Authenticator
{
    private const TableName = 'users';
    private const ColumnId = 'id';
    private const ColumnEmail = 'email';
    private const ColumnPasswordHash = 'password_hash';

    public function __construct(
        private Explorer $database,
        private Passwords $passwords,
    ) {
    }

    public function authenticate(string $user, string $password): SimpleIdentity
    {
        $row = $this->database->table(self::TableName)
            ->where(self::ColumnEmail, $user)
            ->fetch();

        if (!$row) {
            throw new AuthenticationException('The username is incorrect.', self::IDENTITY_NOT_FOUND);
        }

        if (!$this->passwords->verify($password, $row[self::ColumnPasswordHash])) {
            throw new AuthenticationException('The password is incorrect.', self::INVALID_CREDENTIAL);
        }

        if ($this->passwords->needsRehash($row[self::ColumnPasswordHash])) {
            $row->update([
                self::ColumnPasswordHash => $this->passwords->hash($password),
            ]);
        }

        $arr = $row->toArray();
        unset($arr[self::ColumnPasswordHash]);
        return new SimpleIdentity($row[self::ColumnId], null, $arr);
    }

    public function registerUser(string $email, string $password)
    {
        $this->database->table(self::TableName)->insert([
            self::ColumnEmail => $email,
            self::ColumnPasswordHash => $this->passwords->hash($password),
        ]);
    }

    public function changePassword(int $userId, string $newPassword)
    {
    }
}
