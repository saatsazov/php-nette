<?php

namespace App\Presenters;

use App\Forms\ChangePasswordFormFactory;
use App\Forms\LoginFormFactory;
use App\Forms\RegistrationFormFactory;
use App\Model\UserFacade;
use Nette\Application\UI\Form;
use Nette\Application\UI\Presenter;

final class AuthPresenter extends Presenter
{
    public function __construct(
        private UserFacade $userFacade,
        private LoginFormFactory $loginFormFactory,
        private RegistrationFormFactory $registrationFormFactory,
        private ChangePasswordFormFactory $changePasswordFormFactory,
    ) {
    }

    protected function createComponentLoginForm(): Form
    {
        return $this->loginFormFactory->create();
    }

    protected function createComponentRegistrationForm(): Form
    {
        return $this->registrationFormFactory->create();
    }

    public function createComponentChangePasswordForm(): Form
    {
        return $this->changePasswordFormFactory->create();
    }
}
