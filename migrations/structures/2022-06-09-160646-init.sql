create table users
(
    id int not null PRIMARY KEY AUTO_INCREMENT,
    email varchar(255) not null unique,
    password_hash varchar(255) not null
);