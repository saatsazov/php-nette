Known issues:
- [ ] running bin/console does not regenerating configuration from .neon files

Todo:
- [ ] make class for representing form data
- [ ] password confirmation for signup & change password
- [ ] phpstan
- [ ] gitlab pipeline
- [ ] tests & fixture

Apply migrations:
`bin/console migrations:continue`

Run app 
`php -S localhost:8000 -t www`